﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Exchange.Network;
using Exchange.Utils;

using System.Diagnostics;
using System.Threading;

namespace Exchange {
    class Program {
        static void Main(string[] args) {

            Stocks stock = new Stocks();

            while (true) {
                Console.Clear();
                try {
                    Console.WriteLine("Willkommen zur Börse!");
                    Console.WriteLine("Überprüfe Internetverbindung ...");

                    API.Status s = API.GetStatus();
                    if (s == API.Status.Offline) {
                        Console.WriteLine("Bitte überprüfen Sie Ihre Internetverbindung!");
                    } else if (s == API.Status.Unreachable) {
                        Console.WriteLine("Die Börse ist derzeit nicht erreichbar!");
                    } else { Console.WriteLine("Verbunden!"); }

                    Console.WriteLine("Aktualisiere Liste aller Börsennotierten Unternehmen!");
                    stock.Load();

                    Console.WriteLine("Alle Daten wurden erfolgreich geladen!");
                    break;
                } catch { Console.WriteLine("Fehler beim Laden einiger Elemente! Bitte überprüfen Sie Ihre Internetverbindung!"); }
                Console.WriteLine("Klicken Sie eine beliebige Taste, um fortzufahren!");
                Console.ReadKey();
            }

            while(true) {
                try {
                    Console.Clear();

                    //einfach das menü
                    Console.WriteLine(" ## Börsennotierte Unternehmen ## ");
                    Console.WriteLine("1 - Liste der börsennotierte Unternehmen aktualisieren");
                    Console.WriteLine("2 - Anzahl der börsennotierte Unternehmen");
                    Console.WriteLine("3 - Nach Symbol filtern");
                    Console.WriteLine("4 - Nach Name filtern");
                    Console.WriteLine("5 - Nach Industrietyp filtern");
                    Console.WriteLine("6 - Nach Sektor filtern");
                    Console.WriteLine("7 - Alle Industrietypen ausgeben");
                    Console.WriteLine("8 - Alle Sektoren ausgeben");
                    Console.WriteLine("\n ## Börsenwert ##");
                    Console.WriteLine("9 - Börsenwert für Symbol");
                    //Console.WriteLine("10 - Börsenwert für Name");

                    switch (Console.ReadLine()) {

                        case "1":
                            Console.WriteLine("Aktualisiere die Liste der börsennotierten Unternehmen ...");
                            stock.Load();
                            Console.WriteLine("Liste aktualisiert");
                            break;

                        case "2":
                            Console.WriteLine("Anzahl: " + stock.Count());
                            break;

                        case "3":
                            Console.Write("Filter: ");
                            string filter = Console.ReadLine();
                            Console.WriteLine();

                            List<Stocks.StockMember> liste = stock.SearchBySymbol(filter);
                            Console.WriteLine("Anzahl: " + liste.Count);
                            Console.WriteLine(" ## Elemente ##");
                            liste.ForEach((x) => { Console.WriteLine("Symbol: {0}, Name: {1}, Industrietyp: {2}, Sektor: {3}", x.Symbol, x.Name, x.Industry, x.Sector); });
                            break;

                        case "4":
                            Console.Write("Filter: ");
                            filter = Console.ReadLine();
                            Console.WriteLine();

                            liste = stock.SearchByName(filter);
                            Console.WriteLine("Anzahl: " + liste.Count);
                            Console.WriteLine(" ## Elemente ##");
                            liste.ForEach((x) => { Console.WriteLine("Name: {0}, Symbol: {1}, Industrietyp: {2}, Sektor: {3}", x.Name, x.Symbol, x.Industry, x.Sector); });
                            break;

                        case "5":
                            Console.Write("Filter: ");
                            filter = Console.ReadLine();
                            Console.WriteLine();

                            liste = stock.SearchByIndustry(filter);
                            Console.WriteLine("Anzahl: " + liste.Count);
                            Console.WriteLine(" ## Elemente ##");
                            liste.ForEach((x) => { Console.WriteLine("Industrietyp: {0}, Name: {1}, Symbol: {2}, Sektor: {3}", x.Industry, x.Name, x.Symbol, x.Sector); });
                            break;

                        case "6":
                            Console.Write("Filter: ");
                            filter = Console.ReadLine();
                            Console.WriteLine();

                            liste = stock.SearchBySector(filter);
                            Console.WriteLine("Anzahl: " + liste.Count);
                            Console.WriteLine(" ## Elemente ##");
                            liste.ForEach((x) => { Console.WriteLine("Sektor: {0}, Name: {1}, Symbol: {2}, Industrietyp: {3}", x.Sector, x.Name, x.Symbol, x.Industry); });
                            break;

                        case "7":
                            List<string> values = stock.GetAllIndustryTypes();
                            Console.WriteLine("Anzahl: " + values.Count);
                            Console.WriteLine(" ## Elemente ##");
                            values.ForEach((x) => { Console.WriteLine(x); });
                            break;

                        case "8":
                            values = stock.GetAllSectorTypes();
                            Console.WriteLine("Anzahl: " + values.Count);
                            Console.WriteLine(" ## Elemente ##");
                            values.ForEach((x) => { Console.WriteLine(x); });
                            break;

                        case "9":
                            Console.Write("Symbol: ");
                            string symbol = Console.ReadLine();

                            Price p = API.GetTickerForSymbol(symbol);
                            Console.WriteLine(p.Symbol);
                            if (p.Symbol.ToLower() != symbol.ToLower()) {
                                Console.WriteLine("Symbol nicht gefunden!");
                                break;
                            }

                            Console.WriteLine("Kurs für {0} - {1}", p.Symbol, p.Name);
                            Console.WriteLine("Open: {0} - Last: {1} - Bid: {2} - Ask: {3} - PreviousClose: {4}", p.Open, p.Last, p.Bid, p.Ask, p.PreviousClose);
                            break;

                       /* case "10":
                            Console.Write("Name: ");
                            string name = Console.ReadLine();
                            symbol = stock.GetSymbolFromName(name);

                            if(symbol == null) {
                                Console.WriteLine("Name nicht gefunden!");
                                break;
                            }

                            p = API.GetTickerForSymbol(symbol);
                            Console.WriteLine(p.Symbol);
                            if (p.Symbol.ToLower() != symbol.ToLower()) {
                                Console.WriteLine("Symbol nicht gefunden!");
                                break;
                            }

                            Console.WriteLine("Kurs für {0} - {1}", p.Symbol, p.Name);
                            Console.WriteLine("Open: {0} - Last: {1} - Bid: {2} - Ask: {3} - PreviousClose: {4}", p.Open, p.Last, p.Bid, p.Ask, p.PreviousClose);
                            break;*/

                        default:
                            Console.WriteLine("Befehlt nicht gefunden!");
                            break;

                    }
                } catch { Console.WriteLine("Es ist ein Fehler aufgetreten!"); }
                Console.WriteLine("Klicken Sie eine beliebige Taste, um fortzufahren!");
                Console.ReadKey();
            }

        }
    }
}
