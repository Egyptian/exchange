﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exchange.Network {

    public struct Price {
        public string Symbol;
        public string Name;
        public decimal Bid;
        public decimal Ask;
        public decimal Open;
        public decimal PreviousClose;
        public decimal Last;
    }

    internal class ResponseConverter {

        private List<Price> fields;

        public ResponseConverter(string response) {
            //hier wissen wir, dass das eine csv datei ist
            fields = Parse(response);
        }

        public static List<Price> Parse(string response) {
            if (string.IsNullOrWhiteSpace(response)) throw new ArgumentNullException("nope.");
            List<Price> preise = new List<Price>();

            string[] rows = response.Replace("\r", "").Replace(", ", " ").Split('\n');
            foreach (string row in rows) {
                if (string.IsNullOrEmpty(row)) continue;
                string[] cols = row.Split(',');

                int i = 0;
                foreach(string temp in cols) {
                    if(temp == "N/A") { cols[i] = "0"; }
                    cols[i] = cols[i].Replace("\"", "");
                    i++;
                }

                preise.Add(new Price() {
                    Symbol = cols[0],
                    Name = cols[1],
                    Bid = Convert.ToDecimal(cols[2]),
                    Ask = Convert.ToDecimal(cols[3]),
                    Open = Convert.ToDecimal(cols[4]),
                    PreviousClose = Convert.ToDecimal(cols[5]),
                    Last = Convert.ToDecimal(cols[6])
                });
            }

            return preise;
        } //ende Parse

        public Price GetFromName(string name) {
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException("nope.");
            return fields.Find((p) => { return p.Name == name; }); 
        }

        public Price GetFromSymbol(string symbol) {
            if (string.IsNullOrWhiteSpace(symbol)) throw new ArgumentNullException("nope.");
            return fields.Find((p) => { return p.Symbol == symbol; });
        }

    }
}
