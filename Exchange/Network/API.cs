﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;

using Exchange.Utils;

namespace Exchange.Network {

    public static class API {

        public const string HOST = "finance.yahoo.com";

        public enum Status {
            Online, //lol
            Unreachable, //Internet available, but finance is down
            Offline //No internet connection
        }

        public static Status GetStatus(string host = HOST) {
            using (Ping p = new Ping()) {
                try {
                    if (p.Send(host).Status.ToString() == "Success") { return Status.Online; }
                    return Status.Unreachable;
                } catch { return Status.Offline; }
            }
        }

        public static Price GetTickerForSymbol(string symbol) {
            string response;
            using (WebClient web = new WebClient()) { 
                response = web.DownloadString(string.Format("http://{0}/d/quotes.csv?s={1}&f=snbaopl1", HOST, symbol));
            }
            return new ResponseConverter(response).GetFromSymbol(symbol);
        }

        public static List<Stocks.StockMember> GetStockList() {
            if(GetStatus("nasdaq.com") == Status.Online) {
                List<Stocks.StockMember> members = new List<Stocks.StockMember>();

                string response;
                using (WebClient web = new WebClient()) { response = web.DownloadString("http://www.nasdaq.com/screening/companies-by-name.aspx?0&exchange=YAHOO&render=download"); }

                string[] rows = response.Replace("\r", "").Split('\n');
                foreach (string row in rows) {
                    if (string.IsNullOrEmpty(row)) continue;

                    string row2 = row.Replace("\",\"", "~");

                    string[] cols = row2.Split('~');
                    members.Add(new Stocks.StockMember() {
                        Symbol = cols[0].Replace("\"", ""),
                        Name = cols[1],
                        Sector = cols[5],
                        Industry = cols[6]
                    });
                }
                return members;
            }
            return null;
        }

    }
}
