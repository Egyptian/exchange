﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Exchange.Utils;

namespace Exchange.Network {

    public class Stocks {

        public struct StockMember {
            public string Name;
            public string Symbol;
            public string Industry;
            public string Sector;
        }

        private List<StockMember> members;
        public void Load() { members = API.GetStockList(); }

        public List<StockMember> SearchByName(string name) {
            if (members == null) return null;
            return members.FindAll((x) => x.Name.SimilarTo(name)).ToList();
        }
        public List<StockMember> SearchBySymbol(string symbol) {
            if (members == null) return null;
            return members.FindAll((x) => x.Symbol.SimilarTo(symbol)).ToList();
        }
        public List<StockMember> SearchByIndustry(string industry) {
            if (members == null) return null;
            return members.FindAll((x) => x.Industry.SimilarTo(industry)).ToList();

        }
        public List<StockMember> SearchBySector(string sector) {
            if (members == null) return null;
            return members.FindAll((x) => x.Name.SimilarTo(sector)).ToList();
        }

        public int Count() { return members.Count; }

        public List<string> GetAllIndustryTypes() {
            if (members == null) return null;
            return members.Select(x => x.Industry).Distinct().ToList();
        }

        public List<string> GetAllSectorTypes() {
            if (members == null) return null;
            return members.Select(x => x.Sector).Distinct().ToList();
        }

        public string GetSymbolFromName(string name) {
            if (members == null) return null;
            return members.Find(x => x.Name.ToLower() == name.ToLower()).Symbol;
        }

    }

}
